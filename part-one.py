#!/usr/bin/env python3

import os, math

snafu = {
    "2": 2,
    "1": 1,
    "0": 0,
    "-": -1,
    "=": -2
}

def to_int(snafu_value):
    col_val = 1
    int_val = 0
    for i in range(len(snafu_value) - 1, -1, -1):
        int_val += (col_val * snafu[snafu_value[i]])
        col_val *= 5
    return int_val

def to_snafu(int_value):
    abs_val = abs(int_value)
    snafu_bit_count = (math.floor(math.log(abs_val * 2, 5)))
    snafu_answer = ""
    remainder = int_value
    next_bit = snafu_bit_count
    for i in range(snafu_bit_count, -1, -1):
        if i > next_bit:
            snafu_answer += "0"
            continue
        snafu_value = pow(5,i)
        snafu_bit_range = (snafu_value // 2)
        if remainder > 0:
            if (snafu_value - snafu_bit_range) <= remainder <= (snafu_value + snafu_bit_range):
                snafu_answer += "1"
                remainder -= snafu_value
            elif ((snafu_value * 2) - snafu_bit_range) <= remainder <= ((snafu_value * 2) + snafu_bit_range):
                snafu_answer += "2"
                remainder -= (snafu_value * 2)
        elif remainder < 0:
            if (0 - snafu_value + snafu_bit_range) >= remainder >= (0 - snafu_value - snafu_bit_range):
                snafu_answer += "-"
                remainder += snafu_value
            elif (0 - (snafu_value * 2) + snafu_bit_range) >= remainder >= (0 - (snafu_value * 2) - snafu_bit_range):
                snafu_answer += "="
                remainder += (snafu_value * 2)
        next_bit = (math.floor(math.log(abs(remainder) * 2, 5))) if abs(remainder) > 0 else -1
    return snafu_answer


def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    sum = 0
    for line in data:
        sum += to_int(line)

    answer = to_snafu(sum)

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
